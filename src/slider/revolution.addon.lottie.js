import lottie from './lottie.min'
/**
 * @preserve
 * @author    ThemePunch <info@themepunch.com>
 * @link      http://www.themepunch.com/
 * @copyright 2020 ThemePunch
 */
/* eslint-disable */
(function () {
    "use strict";
    var $, win, _R;
    window.LottieAddOn = function (_$, slider, carousel) {
        if (!_$ || !slider) return;
        $ = _$;
        win = $(window);
        $.event.special.rsLottieDestroyed = {
            remove: function (evt) {
                evt.handler()
            }
        };
        return new Lottie(slider, carousel)
    };

    function Lottie(slider, carousel) {
        this.id;
        this.slider = slider;
        this.carousel = carousel;
        slider.one('revolution.slide.onloaded', this.onLoaded.bind(this)).one('rsLottieDestroyed', this.destroy.bind(this))
    }

    Lottie.prototype = {
        onLoaded: function () {
            var slider = this.slider, carousel = this.carousel, isScrollBased = !1, id;
            var opt = $.fn.revolution && $.fn.revolution[slider[0].id] ? $.fn.revolution[slider[0].id] : !1;
            if (!opt) return;
            this.id = id = slider[0].id;
            _R = $.fn.revolution;
            this.revObj = opt;
            this.lottieLayers = slider.find('.tp-lottie').each(function () {
                var $this = $(this), options = $this.attr('data-lottie');
                try {
                    options = JSON.parse(options)
                } catch (e) {
                    return
                }
                var slide = $this.hasClass("rs-layer-static") ? $this.closest('rs-static-layers') : $this.closest('rs-slide');
                if (options.scrollActions === "false") options.scrollActions = !1;
                if (options.interType === "scroll" && options.scrollActions) {
                    options.scrollActions = JSON.parse(options.scrollActions);
                    isScrollBased = !0;
                    options.autoplay = !1
                }
                options.duration = options.duration / 1000;
                var obj = {
                    id: id,
                    options: options,
                    opt: opt,
                    layer: $this,
                    slider: slider,
                    carousel: carousel,
                    wrapper: $this.closest('rs-layer-wrap'),
                    isStatic: $this.hasClass('rs-layer-static'),
                    fullAlign: this.getAttribute('data-basealign') === 'slide',
                    slotholder: slide.find('rs-sbg-wrap'),
                    slideIndex: $this.hasClass("rs-layer-static") ? 9999 : parseInt(slide.attr('data-originalindex'), 10)
                };
                $this.data('lottieObj', obj)
            });
            if (this.lottieLayers.length) {
                if (this.revObj.onBeforeSwap !== undefined) {
                    this.beforeChange.call(this, null, this.revObj.onBeforeSwap)
                }
                slider.on('leftviewport', this.leftViewport.bind(this)).on('enterviewport', this.enterViewport.bind(this)).on('revolution.slide.onbeforeswap', this.beforeChange.bind(this)).on('revolution.slide.afterdraw', this.onResize.bind(this)).on('revolution.slideprepared', this.slidePrepared.bind(this)).on('revolution.slide.layeraction', this.layerAction.bind(this));
                if (isScrollBased) {
                    var $scrollHandler = this.scrollHandler.bind(this);
                    _R[id].c.on('timeline_scroll_processed', function (e, a) {
                        var _top = 0 - (_R[id].scrollProgBasics.top),
                            _full = _R[id].scrollProgBasics.height + _R.lastwindowheight - (_R.lastwindowheight) - _R[id].module.height,
                            prog = Math.min(1, Math.max(0, _top / _full));
                        $scrollHandler(prog)
                    })
                }
                this.revObj.c.on('layeraction', function (e, action, layer, event) {
                    var target = $("#" + event.layer);
                    var lottie = target.data("lottieaddon");
                    if (!lottie) return;
                    switch (action) {
                        case "lottieplay":
                            lottie.play();
                            break;
                        case "lottiepause":
                            lottie.pause();
                            break;
                        case "lottierestart":
                            lottie.restart();
                            break
                    }
                })
            } else {
                this.destroy()
            }
        }, slidePrepared: function (e, data) {
            if (this.waitingForSlidePrepare) {
                this.beforeChange.call(this, null, this.revObj.onBeforeSwap)
            }
        }, scrollHandler: function (prog) {
            var $this = this;
            this.slider.find('rs-slide, rs-static-layers').each(function (i, elem) {
                var isStaticLayer = elem.tagName.toLowerCase() === 'rs-static-layers';
                var isActiveSlide = this === $this.activeSlide;
                $(elem).find(".tp-lottie").each(function () {
                    var $layer = $(this);
                    var lottie = $layer.data('lottieaddon');
                    if (lottie && lottie.scrollActions && (isActiveSlide || isStaticLayer)) {
                        lottie.scrollHandler(prog)
                    }
                })
            })
        }, beforeChange: function (e, data) {
            this.waitingForSlidePrepare = !1;
            if (!(this.revObj.slidePresets && this.revObj.slidePresets[data.nextSlideLIIndex])) {
                this.waitingForSlidePrepare = !0;
                if (!this.carousel) return
            }
            this.slider.find('.tp-lottie').data('lottieIndex', parseInt(data.nextSlideIndex, 10));
            this.activeSlide = data.nextslide[0]
        }, enterViewport: function () {
            var $this = this;
            this.slider.find('rs-slide, rs-static-layers').each(function (i, elem) {
                var isStaticLayer = elem.tagName.toLowerCase() === 'rs-static-layers';
                var isActiveSlide = this === $this.activeSlide;
                $(elem).find(".tp-lottie").each(function () {
                    var $layer = $(this);
                    var lottie = $layer.data('lottieaddon');
                    if (lottie && (isActiveSlide || isStaticLayer)) {
                        lottie.resume()
                    }
                })
            })
        }, leftViewport: function (e) {
            this.slider.find(".tp-lottie").each(function () {
                var $this = $(this);
                var lottie = $this.data('lottieaddon');
                if (lottie) {
                    lottie.pause()
                }
            })
        }, onResize: function (e) {
            clearTimeout(this.resizeTimer);
            this.resizeTimer = setTimeout(this.resize.bind(this), 250)
        }, resize: function () {
            var $this = this;
            this.slider.find('rs-slide, rs-static-layers').each(function (i, elem) {
                var isStaticLayer = elem.tagName.toLowerCase() === 'rs-static-layers';
                var isActiveSlide = this === $this.activeSlide;
                $(elem).find(".tp-lottie").each(function () {
                    var $layer = $(this);
                    var lottie = $layer.data('lottieaddon');
                    if (lottie && (isActiveSlide || isStaticLayer)) {
                        lottie.resize()
                    }
                })
            })
        }, layerAction: function (e, o) {
            var target = o.layer[0];
            var $layer = $(target);
            var lottie = $layer.data('lottieaddon');
            if (!lottie) {
                createLottie.call($layer)
            } else if (lottie) {
                if (o.eventtype === 'enterstage') lottie.reset(lottie.respectTlStart);
                if (o.eventtype === 'enteredstage' && lottie.respectTlStart === !0) lottie.reset()
            } else if (o.eventtype === 'leftstage') {
                lottie.pause()
            }
            if (lottie) {
                lottie.resize()
            }
        }, checkRemoved: function () {
            if (!this.slider || !document.body.contains(this.slider[0])) {
                this.destroy();
                return !0
            }
            return !1
        }, destroy: function () {
            this.slider.find('.tp-lottie').each(function () {
                var $this = $(this), lottie = $this.data('lottieaddon');
                if (lottie) {
                    lottie.destroy()
                }
                $this.removeData('lottieaddon lottieObj')
            });
            for (var prop in this) {
                if (this.hasOwnProperty(prop)) delete this[prop]
            }
        }
    };

    function createLottie() {
        var $this = $(this), lottieObj = $this.data('lottieObj');
        if (!lottieObj) return;
        var newLottie = RsLottieAddon(lottieObj);
        $this.data('lottieaddon', newLottie)
    }

    function RsLottieAddon(obj) {
        var id = obj.id, options = obj.options, url = options.jsonUrl, renderer = options.type,
            rendererClassName = options.type === 'svg' ? 'tp-lottie-svg' : 'tp-lottie-canvas',
            rendererSize = options.size === 'cover' ? 'xMidYMid slice' : 'xMidYMid meet',
            repeat = options.endlessLoop && options.interType !== "morph" ? options.repeat : 0,
            yoyo = options.interType !== "morph" ? options.reverse : !1, animation, frame = {val: 0}, layer = obj.layer,
            rect = layer[0].getBoundingClientRect(),
            mouse = {clicked: !1, entered: !1, x: 0, y: 0, lerp: !1, lerpDone: !0},
            scroll = {lerp: !1, lerpDone: !0, prog: 0}, scrollActions = options.scrollActions, scrollLoop = "",
            scrollLoopAnimation;
        if (options.lerp > 0) mouse.lerp = !0;
        if (options.scrollLerp > 0) scroll.lerp = !0;
        if (yoyo && options.endlessLoop && repeat == 0) repeat = 1;
        var anim;
        if (url !== '') {
            $.ajax({
                url: url, crossOrigin: !0, error: function (e) {
                    console.error("Failed to load url")
                }
            }).done(function (data) {
                try {
                    if (data instanceof Object) {
                        data = data
                    } else {
                        data = JSON.parse(data)
                    }
                } catch (e) {
                    console.error("Failed to parse Lottie data, make sure you are using valid JSON file");
                    return
                }
                if (options.editorEnabled && options.meta) {
                    lottieApplyChanges(data, options.meta)
                }
                renderLottieLayer(data)
            })
        }

        function renderLottieLayer(data) {
            anim = lottie.loadAnimation({
                container: obj.layer[0],
                renderer: renderer,
                loop: !1,
                autoplay: !1,
                animationData: data,
                rendererSettings: {
                    className: rendererClassName,
                    preserveAspectRatio: rendererSize,
                    viewBoxOnly: !0,
                    progressiveLoad: options.progressiveLoad
                }
            });
            anim.addEventListener("error", errorHandler);
            anim.addEventListener("DOMLoaded", createAnimation)
        }

        function createAnimation() {
            var duration = options.duration;
            var adjustedFrame = anim.totalFrames - 1;
            var endFrame = adjustedFrame / anim.frameRate * 1000;
            var startFrame = 0;
            animation = tpGS.gsap.to(frame, {
                val: endFrame,
                repeat: repeat,
                yoyo: yoyo,
                duration: duration,
                ease: "none",
                paused: !0,
                onUpdate: function () {
                    anim.goToAndStop(frame.val, !1)
                }
            });
            if (options.interType !== "disabled" && options.interType !== "scroll") {
                options.autoplay = !1
            } else if (options.interType === "scroll" && options.scrollActions) {
                options.autoplay = !1
            }
            if (options.interType === "click") {
                layer.on("click", clickHandler)
            } else if (options.interType !== "disabled") {
                layer.on("mouseenter", mouseenterHandler);
                layer.on("mousemove", mousemoveHandler);
                layer.on("mouseleave", mouseleaveHandler)
            }
            if (options.autoplay && !options.respectTlStart) {
                animation.play(0)
            }
        }

        function errorHandler(e) {
            console.error(e.type, e.nativeError.message, e.nativeError.stack)
        }

        var scrollHandler = function (prog) {
            scroll.currentProg = prog;
            if (!scroll.lerp) {
                scroll.prog = prog;
                scrollEase()
            } else if (scroll.lerp && scroll.lerpDone) {
                scroll.lerpDone = !1;
                tpGS.gsap.ticker.add(scrollLerpHandler)
            }
        };
        var scrollLerpHandler = function () {
            if (!animation) return;
            var nProgress = scroll.currentProg;
            if (Math.abs(nProgress - scroll.prog) < 0.0001) {
                scroll.lerpDone = !0;
                tpGS.gsap.ticker.remove(scrollLerpHandler)
            } else {
                scroll.prog += (nProgress - scroll.prog) * options.scrollLerp
            }
            scrollEase()
        }
        var scrollEase = function () {
            if (animation) {
                var progress = 0;
                for (var i in options.scrollActions) {
                    var action = options.scrollActions[i];
                    var type = action.type;
                    if (scroll.prog >= action.progress.start && scroll.prog <= action.progress.end) {
                        if (type === "seek") {
                            var frame = tpGS.gsap.utils.mapRange(action.progress.start, action.progress.end, action.frames.start, action.frames.end, scroll.prog);
                            progress = frame / anim.totalFrames;
                            if (scrollLoopAnimation) scrollLoopAnimation.kill();
                            scrollLoop = "";
                            animation.progress(progress)
                        } else if (type === "loop" && scrollLoop !== action.frames.start + " " + action.frames.end) {
                            var duration = Math.abs(action.frames.start - action.frames.end) / anim.frameRate
                            scrollLoop = action.frames.start + " " + action.frames.end;
                            scrollLoopAnimation = tpGS.gsap.fromTo(animation, {progress: action.frames.start / anim.totalFrames}, {
                                duration: duration,
                                progress: action.frames.end / anim.totalFrames,
                                repeat: -1
                            })
                        } else if (type === "stop") {
                            var frame = action.frames.start;
                            progress = frame / anim.totalFrames;
                            if (scrollLoopAnimation) scrollLoopAnimation.kill();
                            scrollLoop = "";
                            animation.progress(progress)
                        }
                        break
                    }
                }
            }
        }
        var clickHandler = function () {
            if (animation && !animation.isActive()) {
                animation.play(0)
            }
        };
        var mouseenterHandler = function (e) {
            if (options.interType === "morph") {
                play()
            } else if (options.interType === "hover") {
                play()
            } else if (options.interType === "mousemove") {
                mouse.entered = !0;
                rect = this.getBoundingClientRect();
                if (mouse.lerp && mouse.lerpDone) {
                    mouse.lerpDone = !1;
                    tpGS.gsap.ticker.add(lerpHandler)
                }
                animation.pause()
            }
        };
        var mousemoveHandler = function (e) {
            if (options.interType !== "mousemove") return;
            if (!mouse.entered) {
                rect = this.getBoundingClientRect();
                mouse.entered = !0;
                if (mouse.lerp) {
                    tpGS.gsap.ticker.add(lerpHandler)
                }
            }
            mouse.x = e.clientX - rect.left;
            mouse.y = e.clientY - rect.top;
            if (mouse.lerp && mouse.lerpDone) {
                mouse.lerpDone = !1;
                tpGS.gsap.ticker.add(lerpHandler)
            }
            if (!mouse.lerp) {
                if (!mouse.frame) mouse.frame = window.requestAnimationFrame(setProgress)
            }
        };
        var mouseleaveHandler = function (e) {
            if (options.interType === "morph") {
                animation.reverse()
            } else if (options.interType === "hover") {
            } else if (options.interType === "mousemove") {
                mouse.x = e.clientX - rect.left;
                mouse.y = e.clientY - rect.top;
                mouse.entered = !1;
                if (mouse.x <= 0) {
                    mouse.x = 0
                } else if (mouse.x >= rect.width) {
                    mouse.x = rect.width
                }
                if (options.continuePlaying) {
                    mouse.lerpDone = !0;
                    tpGS.gsap.ticker.remove(lerpHandler);
                    animation.play()
                }
            }
        };
        var lerpHandler = function () {
            if (!animation) return;
            var nProgress = mouse.x / rect.width;
            var progress = animation.progress();
            if (Math.abs(nProgress - progress) < 0.0001) {
                animation.progress(nProgress);
                mouse.lerpDone = !0;
                tpGS.gsap.ticker.remove(lerpHandler)
            } else {
                animation.progress(progress + (nProgress - progress) * options.lerp)
            }
        }
        var setProgress = function () {
            if (!animation) return;
            if (mouse.frame) mouse.frame = window.cancelAnimationFrame(mouse.frame);
            animation.progress(mouse.x / rect.width)
        }
        var reset = function (onlyReset) {
            if (!animation) return;
            if (options.autoplay) {
                animation.play(0)
            } else {
                animation.progress(0)
            }
            if (onlyReset) animation.progress(0).pause()
        }
        var restart = function () {
            if (!animation) return;
            animation.play(0)
        }
        var play = function () {
            if (animation && !animation.isActive()) {
                if (animation.progress() === 1 || animation.progress() === 0) animation.play(0)
                else animation.play()
            }
        };
        var pause = function () {
            if (animation) animation.pause()
        }
        var resize = function () {
            if (anim) anim.resize()
        };
        var resume = function () {
            if (animation && options.autoplay) animation.resume()
        };
        var destroy = function () {
            layer.off("click", clickHandler);
            layer.off("mouseenter", mouseenterHandler);
            layer.off("mousemove", mousemoveHandler);
            layer.off("mouseleave", mouseleaveHandler);
            if (animation) {
                animation.kill();
                animation = null
            }
            options = null;
            anim.removeEventListener("data_ready", createAnimation);
            anim.removeEventListener("error", errorHandler);
            anim.destroy();
            anim = null
        }
        var addonObj = {
            animation: animation,
            scrollActions: options.interType === "scroll" ? scrollActions : !1,
            respectTlStart: options.respectTlStart,
            scrollHandler: scrollHandler,
            reset: reset,
            play: play,
            restart: restart,
            pause: pause,
            resume: resume,
            resize: resize,
            destroy: destroy
        };
        return addonObj
    }

    function lottieApplyChanges(data, meta) {
        var w = data.w;
        var h = data.h;
        for (var i in meta.layers) {
            if (!meta.layers.hasOwnProperty(i)) continue;
            var layer = meta.layers[i];
            for (var j in layer.fields) {
                if (!layer.fields.hasOwnProperty(j)) continue;
                var f = layer.fields[j];
                var arr = f.path.split('.');
                var field = data;
                var isField = !1;
                for (var k = 0; k < arr.length; k++) {
                    field = field[arr[k]];
                    if (field === undefined) {
                        break
                    } else if (k === arr.length - 1) isField = !0
                }
                if (isField) applyChanges(field, f)
            }
        }

        function applyChanges(field, changes) {
            if (changes.RSChanged) field.RSChanged = !0;
            if (changes.isEffect) {
                field.v.k = changes.val
            } else {
                if ((field.c && field.c.a === 0) || (field.g && field.g.k && field.g.k.a === 0)) {
                    if (changes.type === 'st' || changes.type === 'fl') {
                        field.c.k = changes.val;
                        field.ty = changes.type
                    } else if (changes.type === 'gs' || changes.type === 'gf') {
                        field.ty = changes.type;
                        field.ty = f.type;
                        field.t = f.gradType;
                        field.g = changes.val;
                        if (field.s === undefined) {
                            field.s = {a: 0, k: [-w / 2, -h / 2]}
                        }
                        if (field.e === undefined) {
                            field.e = {a: 0, k: [w / 2, h / 2]}
                        }
                        if (changes.s && field.s && field.s.a === 0) {
                            field.s.k = changes.s
                        }
                        if (changes.e && field.e && field.e.a === 0) {
                            field.e.k = changes.e
                        }
                        if (changes.rsg) {
                            field.rsg = changes.rsg
                        }
                    }
                }
                if (changes.stroke && field.w && field.w.a === 0) field.w.k = changes.stroke
            }
        }
    }
})()