import jQuery from 'jquery'
import './rs6.min'
import './custom.min'
import './revolution.addon.lottie'
import './revolution.addon.scrollvideo'


/*
    <script type='text/javascript' src='./slider/js/custom.min.js' id='custom-js'></script>
<script type='text/javascript' src='./slider/js/lottie.min.js' id='lottie-js'></script>
<script data-minify="1" type='text/javascript' src='./slider/js/revolution.addon.lottie.js'
        id='rs-lottie-front-js'></script>
<script data-minify="1" type='text/javascript' src='./slider/js/revolution.addon.scrollvideo.js'
        id='rs-scrollvideo-front-js'></script>
*/


function setREVStartSize(e) {
    //window.requestAnimationFrame(function() {
    window.RSIW = window.RSIW === undefined ? window.innerWidth : window.RSIW;
    window.RSIH = window.RSIH === undefined ? window.innerHeight : window.RSIH;
    try {
        var pw = document.getElementById(e.c).parentNode.offsetWidth,
            newh;
        pw = pw === 0 || isNaN(pw) ? window.RSIW : pw;
        e.tabw = e.tabw === undefined ? 0 : parseInt(e.tabw);
        e.thumbw = e.thumbw === undefined ? 0 : parseInt(e.thumbw);
        e.tabh = e.tabh === undefined ? 0 : parseInt(e.tabh);
        e.thumbh = e.thumbh === undefined ? 0 : parseInt(e.thumbh);
        e.tabhide = e.tabhide === undefined ? 0 : parseInt(e.tabhide);
        e.thumbhide = e.thumbhide === undefined ? 0 : parseInt(e.thumbhide);
        e.mh = e.mh === undefined || e.mh == "" || e.mh === "auto" ? 0 : parseInt(e.mh, 0);
        if (e.layout === "fullscreen" || e.l === "fullscreen")
            newh = Math.max(e.mh, window.RSIH);
        else {
            e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
            for (let i in e.rl) if (e.gw[i] === undefined || e.gw[i] === 0) e.gw[i] = e.gw[i - 1];
            e.gh = e.el === undefined || e.el === "" || (Array.isArray(e.el) && e.el.length == 0) ? e.gh : e.el;
            e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
            for (let i in e.rl) if (e.gh[i] === undefined || e.gh[i] === 0) e.gh[i] = e.gh[i - 1];

            var nl = new Array(e.rl.length),
                ix = 0,
                sl;
            e.tabw = e.tabhide >= pw ? 0 : e.tabw;
            e.thumbw = e.thumbhide >= pw ? 0 : e.thumbw;
            e.tabh = e.tabhide >= pw ? 0 : e.tabh;
            e.thumbh = e.thumbhide >= pw ? 0 : e.thumbh;
            for (let i in e.rl) nl[i] = e.rl[i] < window.RSIW ? 0 : e.rl[i];
            sl = nl[0];
            for (let i in nl) if (sl > nl[i] && nl[i] > 0) {
                sl = nl[i];
                ix = i;
            }
            var m = pw > (e.gw[ix] + e.tabw + e.thumbw) ? 1 : (pw - (e.tabw + e.thumbw)) / (e.gw[ix]);
            newh = (e.gh[ix] * m) + (e.tabh + e.thumbh);
        }
        if (window.rs_init_css === undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));
        document.getElementById(e.c).height = newh + "px";
        window.rs_init_css.innerHTML += "#" + e.c + "_wrapper { height: " + newh + "px }";
    } catch (e) {
        console.log("Failure at Presize of Slider:" + e)
    }
    //});
}

setREVStartSize({
    c: 'rev_slider_1030_1',
    rl: [1500, 1240, 778, 480],
    el: [900, 768, 960, 720],
    gw: [1240, 1024, 778, 480],
    gh: [900, 768, 960, 720],
    type: 'hero',
    justify: '',
    layout: 'fullscreen',
    offsetContainer: '#masthead',
    offset: '',
    mh: "0"
});
var revapi1030,
    tpj;

function revinit_revslider10301() {
    tpj = jQuery;
    revapi1030 = tpj("#rev_slider_1030_1");

    revapi1030.revolution({
        sliderType: "hero",
        sliderLayout: "fullscreen",
        visibilityLevels: "1500,1240,778,480",
        gridwidth: "1240,1024,778,480",
        gridheight: "900,768,960,720",
        perspective: 600,
        perspectiveType: "global",
        lazyloaddata: "lazy-src",
        editorheight: "900,768,960,720",
        responsiveLevels: "1500,1240,778,480",
        fullScreenOffsetContainer: "#masthead",
        overlay: {
            type: 8,

        },
        progressBar: {disableProgressBar: true},
        navigation: {
            onHoverStop: false
        },
        sbtimeline: {
            set: true,
            ease: "power2.out",
            speed: "2000ms",
            fixed: true,
            fixStart: "300ms",
            fixEnd: "12000ms"
        },
        fallbacks: {
            allowHTML5AutoPlayOnAndroid: true
        },
    });

    window.LottieAddOn(jQuery, revapi1030, false);

    if (typeof window.RevSliderScrollvideo !== "undefined") window.RevSliderScrollvideo(revapi1030, {
        mp4: "",
        winoffset: "0%",
        ver: "57945",
        winoffsetend: "100%",
        spinner: false,
        scroll: false,
        dir: "/slider/img",
        first: 1,
        last: 153,
        fps: 10,
        quality: 0.5
    });

    window.sliderLoaded = true;
} // End of RevInitScript

let once_revslider10301 = false;

if (!once_revslider10301) {
    revinit_revslider10301();
}

/*
jQuery(document).ready(() => {

});*/
