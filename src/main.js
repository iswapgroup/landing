import { createApp } from 'vue'
import App from './App.vue'
import VueSmoothScroll from 'vue3-smooth-scroll'

const app = createApp(App)
app.use(VueSmoothScroll)

app.config.globalProperties.insiderApi = 'https://insiderprotocol.com/api/';
//app.config.globalProperties.insiderApi = 'http://127.0.0.1:8000/api/';


app.mount('#app')