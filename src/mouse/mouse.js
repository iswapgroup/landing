import jQuery from 'jquery'

const START_POSITION = -100;
let clientX = null;
let clientY = null;
let xPos = null;
let yPos = null;
let dX = null;
let dY = null;
let lastRunTime = null;

export default new class CustomMouse {
    constructor() {
        if (!this.isMobile()) {
            this.initPositions();
            this.initCursor();
        }
    }

    isMobile() {
        return /Mobi|Android/i.test(navigator.userAgent)
    }

    initPositions() {
        clientX = START_POSITION;
        clientY = START_POSITION;
        xPos = START_POSITION;
        yPos = START_POSITION;
        dX = START_POSITION;
        dY = START_POSITION;
        lastRunTime = Date.now();
    }

    initCursor() {
        const tickPos = 2;
        const fps = 1000 / 60;

        const $cursor = jQuery('<div class="mpl-cursor"></div>');
        const $cursorOuter = jQuery('<div class="mpl-cursor-outer"></div>');

        jQuery('body').append([$cursor, $cursorOuter]).addClass('mpl-cursor-enabled');

        jQuery(document).on('mousemove', (e) => {
            clientX = e.clientX;
            clientY = e.clientY;
        });

        jQuery(document).on('mouseenter', 'a, button, input, textarea, [role="button"]', () => {
            $cursor.addClass('mpl-cursor-hover');
            $cursorOuter.addClass('mpl-cursor-hover');
        }).on('mouseleave', 'a, button, input, textarea, [role="button"]', () => {
            $cursor.removeClass('mpl-cursor-hover');
            $cursorOuter.removeClass('mpl-cursor-hover');
        });

        // Move cursor.
        const moveCursor = () => {
            const now = Date.now();
            const delay = now - lastRunTime;
            lastRunTime = now;

            // First run.
            if (xPos === START_POSITION) {
                dX = clientX;
                dY = clientY;
                xPos = clientX;
                yPos = clientY;
            } else {
                dX = clientX - xPos;
                dY = clientY - yPos;
                xPos += dX / (tickPos * fps / delay);
                yPos += dY / (tickPos * fps / delay);
            }

            $cursor.css('transform', `matrix(1, 0, 0, 1, ${clientX}, ${clientY}) translate3d(0,0,0)`);
            $cursorOuter.css('transform', `matrix(1, 0, 0, 1, ${xPos}, ${yPos}) translate3d(0,0,0)`);

            requestAnimationFrame(moveCursor);
        };

        requestAnimationFrame(moveCursor);
    }
}